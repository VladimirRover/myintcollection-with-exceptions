package fixedsizecollection;

import java.util.ArrayList;

/**
 * Created by User on 08.05.2017
 */

public class FixedSizeCollection extends ArrayList {

    private static int DEFAULT_SIZE ;

    /**
     Default constructor
     The collection size is 16
     */
    public FixedSizeCollection(){
        DEFAULT_SIZE = 16 - 1;
    }

    /**
     Custom constructor
     The size is set by the user
     */
    public FixedSizeCollection(int n) {
        DEFAULT_SIZE = n - 1;
    }

    /**
     The method deletes the first item
     if the collection is overfull
     */
    @Override
    public boolean add(Object o)
    {
        if (super.size() > DEFAULT_SIZE)
        {
            super.remove(super.get(0));
        }
        return super.add(o);
    }
}

class Test{
    public static void main(String[] args) {

        //You can delete the parameter to set the default value
        FixedSizeCollection fixedSizeCollection = new FixedSizeCollection(3);

        fixedSizeCollection.add("a");
        fixedSizeCollection.add("b");
        fixedSizeCollection.add("c");
        fixedSizeCollection.add("d");
        fixedSizeCollection.add("e");
        fixedSizeCollection.add("f");
        fixedSizeCollection.add("j");
        fixedSizeCollection.add("h");
        System.out.println(fixedSizeCollection);

    }

}
