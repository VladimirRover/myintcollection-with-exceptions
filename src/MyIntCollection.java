import exceptionsPackage.CheckBorderException;
import exceptionsPackage.IsPositiveException;

import java.util.ArrayList;

/**
 * Created by User on 01.05.2017.
 */
public class MyIntCollection extends ArrayList{

    /**This method adds an element to the
       collection and increases the values of
       all elements by its value
     */
    @Override
    public boolean add(Object o) {

        if (o instanceof Integer) {
            for (int i = 0; i < super.size(); i++) {

                int n = (Integer) super.set(i, (Integer) super.get(i) + (Integer) o);
            }
        }
        else {
            throw new IllegalArgumentException("The value is not Integer");
        }
        return super.add(o);
    }


    /**This method adds an element to the
       collection at the specified index
       and increases the values of
       all elements by its value
     */
    @Override
    public void add(int k, Object o) {
        if (o instanceof Integer) {
            for (int i = 0; i < super.size(); i++) {

                int n = (Integer) super.set(i, (Integer) super.get(i) + (Integer) o);
            }
        }
        else {
            throw new IllegalArgumentException("The value is not Integer");
        }
        super.add(k, o);
    }


    /**
     This method deletes the specified item.
     All items in the collection reduce their
     value by the amount of the item being deleted.
     */
    @Override
    public boolean remove(Object o) {

        int index = indexOf(o);

        int value = (Integer) super.get(index);

        if (o instanceof Integer) {

            for (int i = 0;  i < super.size(); i++) {
                if (i != index) {
                    int n = (Integer) super.set(i, (Integer) super.get(i) - value);
                }
            }
        }
        else {
            throw new IllegalArgumentException("The value is not Integer");
        }
        super.remove(index);
        return true;
    }

    /**
     This method deletes the specified item by index.
     All items in the collection reduce their
     value by the amount of the item being deleted.
     */
    @Override
    public Object remove(int k) {

            for (int i = 0; i < super.size(); i++) {

                if (i != k ) {
                    int n = (Integer) super.set(i, (Integer) super.get(i) - (Integer) super.get(k));
                }
            }

        super.remove(k);
        return true;
    }


    //Returns the index of the first matched element
    public Object searchByValue(int value) throws CheckBorderException //Fictitious check as runtime on check
    {
        if (value < Integer.MIN_VALUE || value > Integer.MAX_VALUE)
        {
            throw new CheckBorderException("The value out from the borders", value);
        }
        return super.indexOf(value);
    }

    //Returns the first element by index
    public int searchById(int id) throws IsPositiveException {

        if (id < 0){
            throw new IsPositiveException("The index can be only positive. id = " + id );
        }
        return (Integer) super.get(id);
    }

    //Returns the maximum element of a collection
    public int max(){
        int max = (Integer)super.get(0) ;

        for (int i = 0; i < super.size() ; i++) {

            if (max < (Integer) super.get(i)){
                max = (Integer) super.get(i);
            }
        }
        return  max;
    }

    //Returns the minimum element of a collection
    public int min(){
        int min = (Integer)super.get(0) ;

        for (int i = 0; i < super.size() ; i++) {

            if (min > (Integer) super.get(i)){
                min = (Integer) super.get(i);
            }
        }
        return  min;
    }

    //Returns the arithmetic mean of the collection
    public int average(){
        int average = 0;
        for (int i = 0; i < super.size() ; i++) {
            average = average + (Integer) super.get(i);
        }

        return average/super.size();
    }
}


class Test{
    public static void main(String[] args) {
        MyIntCollection myIntCollection = new MyIntCollection();

        myIntCollection.add(1);
        System.out.println(myIntCollection.toString());

        myIntCollection.add(1);
        System.out.println(myIntCollection.toString());

        myIntCollection.add(1);
        System.out.println(myIntCollection.toString());

        myIntCollection.add(1);
        System.out.println(myIntCollection.toString());

        myIntCollection.add(2);
        System.out.println(myIntCollection.toString());


        myIntCollection.remove((Integer)3);
        System.out.println("remove by value "  + myIntCollection);

//        myIntCollection.remove(2);
//        System.out.println("remove by index" + myIntCollection);


        System.out.println("max element " + myIntCollection.max());
        System.out.println("min element " + myIntCollection.min());
        System.out.println("average " + myIntCollection.average());



        try {
            System.out.println("search by id " + myIntCollection.searchById(-1));
        }
        catch (IsPositiveException e){
            System.out.println(e.getMessage());
        }

        try {
            System.out.println("search by value " + myIntCollection.searchByValue(3));
        }
        catch (CheckBorderException e){
            System.out.println(e.getMessage());
        }


    }
}