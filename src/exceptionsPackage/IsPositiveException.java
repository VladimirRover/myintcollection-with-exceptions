package exceptionsPackage;

/**
 * Created by User on 13.05.2017.
 *
 * Checks the index for its positive value
 */
public class IsPositiveException extends Exception{

    public IsPositiveException(String s) {
        super(s);
    }

}
