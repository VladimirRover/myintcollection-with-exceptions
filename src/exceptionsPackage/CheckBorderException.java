package exceptionsPackage;

/**
 * Created by User on 13.05.2017.
 *
   Checks whether the value is within
   the bounds of natural numbers int
 *
 */


public class CheckBorderException extends Exception{
    private int value;
    public CheckBorderException(String s, int value) {
        super(s);
        this.value = value;
    }
}
